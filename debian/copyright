Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Pyblosxom
Source: http://pyblosxom.github.io/

Files: *
Copyright: 2003-2011, The Pyblosxom team
License: Expat
Comment:
 Please refer to the AUTHORS file in the source package for a complete list
 of contributors for Pyblosxom.

Files: Pyblosxom/plugins/check_javascript.py
       Pyblosxom/plugins/akismetcomments.py
Copyright: 2006, Ryan Barrett
           2006, Benjamin Mako Hill <mako@atdot.cc>
           2006, Blake Winton <bwinton+blog@latte.ca>
           2011, Will Kahn-Greene
License: GPL-2+
 On Debian systems, the complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".

Files: Pyblosxom/plugins/markdown_parser.py
Copyright: 2005, 2011 Benjamin Mako Hill
           2009, 2010, seanh
           2011, Blake Winton
           2011, Will Kahn-Greene
License: GPL-3+
 On Debian systems, the complete text of the GNU General Public License version 3
 can be found in "/usr/share/common-licenses/GPL-3".

Files: Pyblosxom/plugins/no_old_comments.py
Copyright: 2006, Blake Winton
License: public-domain
 Released into the Public Domain.

Files: debian/*
Copyright: 2003,      Colin Walters <walters@debian.org>
           2003,      Matthias Klose <doko@debian.org>
           2003-2004, Fredrik Steen <stone@debian.org>
           2005,      Charles Majola <charles@ubuntu.com>
           2005,      Tollef Fog Heen <tfheen@debian.org>
           2006-2008, Norbert Tretkowski <nobse@debian.org>
           2008,      Daniel Watkins <daniel@daniel-watkins.co.uk>
           2013-2018, Markus Koschany <apo@debian.org>
License: Expat


License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
